let content = document.querySelector("#content");
let gameStatus = document.querySelector('#gameStatus');
let trys;
const wordList = ['bad', 'uncertainty', 'unit', 'jury', 'permanent'];
let word;

function printGame() {
    gameStatus.innerText = "Running!";
    document.querySelector("#start").classList.add("hide");
    document.querySelector("#restart").classList.remove("hide");
    content.classList.remove("hide");
    gameStatus.classList.remove("hide");
    document.querySelector('#trycount').classList.remove("hide");
    trys = 5;
    document.querySelector('#trycount').innerText = "Lifes: " + trys;
    word = createWord(randomWord());
    let wordZone = document.createElement('div');
    wordZone.style.display = "flex";
    wordZone.style.width = "max-content";
    wordZone.id = "word";
    content.append(wordZone);
    createChars(wordZone);
    let keyboardZone = document.createElement("div");
    keyboardZone.style.width = "400px";
    keyboardZone.id = "keyboard";
    content.append(keyboardZone);
    createKeyboard(keyboardZone);
}

function newGame() {
    gameStatus.innerText = "Running!";
    trys = 5;
    document.querySelector('#trycount').innerText = "Lifes: " + trys;
    word = createWord(randomWord());
    let wordZone = document.querySelector('#word');
    wordZone.innerHTML = "";
    createChars(wordZone);
    let keyboardZone = document.querySelector('#keyboard');
    keyboardZone.innerHTML = "";
    createKeyboard(keyboardZone);
}

function createChars(wordZone) {
    for (var char of word) {
        let charDiv = document.createElement('div');
        charDiv.style.borderBottom = "1px solid black";
        charDiv.style.width = "30px";
        charDiv.style.height = "23px";
        charDiv.style.margin = "0px 5px";
        charDiv.style.padding = "0px";
        let p = document.createElement("p");
        p.innerText = char;
        p.style.padding = "0px";
        p.classList.add("hiddenChar");
        p.classList.add("chars");
        charDiv.append(p);
        wordZone.append(charDiv);
    }
}

function createWord(word) {
    let chars = [];
    for (var i = 0; i < word.length; i++) {
        chars[i] = word.charAt(i).toUpperCase();
    }
    return chars;
}

function randomWord() {
    let i = Math.floor(Math.random() * wordList.length);
    return wordList[i];
}

function createKeyboard(keyboardZone) {
    let keyboardChars = "QWERTYUIOPASDFGHJKLÇZXCVBNM";
    for (var i = 0; i < keyboardChars.length; i++) {
        let bt = document.createElement('button');
        bt.innerText = keyboardChars.charAt(i);
        bt.style.width = "40px";
        bt.classList.add("charBt");
        bt.addEventListener('click', () => {
            if (!bt.classList.contains("pressed")) {
                word.includes(bt.innerText) ? (bt.style.backgroundColor = "green", displayChars(bt.innerText)) : (bt.style.backgroundColor = "red", trys--, document.querySelector('#trycount').innerText = "Lifes: " + trys);
                bt.classList.add("pressed");
                verifyGameStatus()
            }
        })
        keyboardZone.append(bt);
    }
}

function displayChars(text) {
    let chars = document.querySelectorAll("p.chars");
    for (char of chars) {
        if (char.innerText == text) {
            char.classList.remove("hiddenChar");
        }
    }
}

function verifyGameStatus() {
    let chars = document.querySelectorAll("p.chars");
    let keyboardZone = document.querySelector('#keyboard');
    let n = 0;
    for (char of chars) {
        if (char.classList.contains("hiddenChar")) {
            n++
        }
    }
    n == 0 ? (gameStatus.innerText = "Winner!", keyboardZone.innerHTML = "") : false;
    if (trys == 0) {
        gameStatus.innerText = "Loser!"
        keyboardZone.innerHTML = ""
        for (char of chars) {
            char.classList.remove("hiddenChar");
        }
    }
}
